#include "llc.h"

void llc_tran_request(uint8_t source_addr, uint8_t dest_addr, void *llc_sdu, int csma_mode, uint8_t ack)
{
	// Physical tx
}

uint8_t llc_tran_indication(void *llc_sdu, uint8_t ack)
{
	return 0;
}

uint8_t llc_tran_confirm(uint8_t *failure_code)
{
	return 0;
}

void llc_establish_request(uint8_t source_addr, uint8_t dest_addr, int csma_mode)
{

}

uint8_t llc_establish_indication(uint8_t source_addr, uint8_t dest_addr)
{
	return 0;
}

uint8_t llc_establish_response(uint8_t source_addr, uint8_t dest_addr, int csma_mode, int communication_response)
{
	return 0;
}

uint8_t llc_establish_confirm(uint8_t source_addr, uint8_t dest_addr, int communication_response)
{
	return 0;
}


