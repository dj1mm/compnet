#include "phy.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>

#include <string.h>

#include "include/rfm12_hw.h"
#if RFM12_UART_DEBUG
#include "uart.h"
#endif

#include "include/rfm12_spi.c"
#include "include/rfm12_spi_linux.c"

#if RFM12_USE_RX_CALLBACK
static void (*rfm12_rx_callback_func)(void *, uint8_t) = 0x0000;
void phy_rx_setCallback(void(*in_func)(void *, uint8_t)) {
	rfm12_rx_callback_func = in_func;
}
#endif

phy_buffer_t tx_buffer;
phy_buffer_t rx_buffer[2];
rfm12_control_t ctrl;

ISR(RFM12_INT_VECT, ISR_NOBLOCK) {

	put_ch('0');
	RFM12_INT_OFF();

	uint8_t status;
	uint8_t recheck_interrupt;

	do {
		//clear AVR int flag
		RFM12_INT_FLAG = (1<<RFM12_FLAG_BIT);

		// first we read the first byte of the status register
		// to get the interrupt flags
		status = rfm12_read_int_flags_inline();

		// if we use at least one of the status bits, we need to check the
		// status again for the case in which another interrupt condition
		// occured while we were handeling the first one.
		recheck_interrupt = 0;

#if RFM12_UART_DEBUG >= 2
		put_ch('S');
		put_ch(status);
#endif

		// check if the fifo interrupt occurred
		if (status & (RFM12_STATUS_FFIT>>8)) {
			//yes
			recheck_interrupt = 1;
			//see what we have to do (start rx, rx or tx)
			switch (ctrl.rfm12_state) {
				case STATE_RX_IDLE: {
					uint8_t data;

					data = rfm12_read(RFM12_CMD_READ);

					ctrl.bytecount = 1;
					ctrl.num_bytes = 0;

#if RFM12_UART_DEBUG >= 2
					put_ch('I');
#endif

					if (rx_buffer[ctrl.buffer_rx_in].status == BUFFER_FREE &&
						data == FRAME_HEADER_BYTE)
					{
						ctrl.rfm12_state = STATE_RX_ACTIVE;

						goto no_fifo_reset;
					}
				}

				break;

				case STATE_RX_ACTIVE: {
				
					uint8_t data;
					
					data = rfm12_read(RFM12_CMD_READ);
					
#if RFM12_UART_DEBUG >= 2
					put_ch('R');
#endif

					// If we just received an escape byte, whatever we are 
					// receiving should be part of the data
					if (ctrl.escape_byte_received == 1) {
						(&rx_buffer[ctrl.buffer_rx_in].length)[ctrl.bytecount] = data;
						ctrl.escape_byte_received = 0;
					// If we receive an escape byte, next byte should be data
					} else if (data == FRAME_ESCAPE_BYTE) {
						ctrl.escape_byte_received = 1;
					// If we receive the footer, this is the end of this packet
					} else if (data == FRAME_FOOTER_BYTE) {
#if RFM12_USE_RX_CALLBACK
						if (rfm12_rx_callback_func != 0x0000) {
							rfm12_rx_callback_func(rx_buffer[ctrl.buffer_rx_in].data, rx_buffer[ctrl.buffer_rx_in].length);
						}
						rx_buffer[ctrl.buffer_rx_in].status = BUFFER_FREE;
#else
						rx_buffer[ctrl.buffer_rx_in].status = BUFFER_COMPLETED;
						ctrl.buffer_rx_in ^= 1;
						
						break;
#endif
					// Otherwise, the byte we receive is just plain data
					} else {
						(&rx_buffer[ctrl.buffer_rx_in].length)[ctrl.bytecount] = data;
					}
					
					ctrl.bytecount++;
					goto no_fifo_reset;
				}

				break;

				case STATE_TX: {
				
					uint8_t data;
					
#if RFM12_UART_DEBUG >= 2
					put_ch('T');
#endif

					if (ctrl.bytecount < ctrl.num_bytes) {

						// load the next byte from our buffer struct.
						data = tx_buffer.data[ctrl.bytecount];
						
						// If this is the first byte to be txed, this should normally
						// be the header. So send the header byte
						if(ctrl.bytecount == 1) {
							rfm12_data( RFM12_CMD_TX | FRAME_HEADER_BYTE );
							// If this is the last byte to be txed, this should be the
							// footer (by right). So send the footer
							
							//end the interrupt without resetting the fifo
							goto no_fifo_reset;
						} else if(ctrl.bytecount == ctrl.num_bytes-1) {
							rfm12_data( RFM12_CMD_TX | FRAME_FOOTER_BYTE );
							// If we are in the middle of transmission and the byte to
							// be transmitted is something special, we escape this byte
							
							//end the interrupt without resetting the fifo
							goto no_fifo_reset;
						} else if(data == FRAME_HEADER_BYTE || data == FRAME_ESCAPE_BYTE || data == FRAME_FOOTER_BYTE) {
							rfm12_data( RFM12_CMD_TX | FRAME_ESCAPE_BYTE );
							// Otherwise, we just transmit that byte and move on
						} else {
							rfm12_data( RFM12_CMD_TX | data);
							ctrl.bytecount++;
							
							//end the interrupt without resetting the fifo
							goto no_fifo_reset;
						}

					}

					/* if we're here, we're finished transmitting the bytes */
					/* the fifo will be reset at the end of the function */

					//flag the buffer as free again
					tx_buffer.status = BUFFER_FREE;

					//turn off the transmitter and enable receiver
					//the receiver is not enabled in transmit only mode (by PWRMGT_RECEIVE makro)
#if RFM12_PWRMGT_SHADOW
					ctrl.pwrmgt_shadow &= ~(RFM12_PWRMGT_ET); /* disable transmitter */
					ctrl.pwrmgt_shadow |= (PWRMGT_RECEIVE);   /* activate predefined receive mode */
					rfm12_data(ctrl.pwrmgt_shadow);
#else
					rfm12_data( PWRMGT_RECEIVE );
#endif

					//load a dummy byte to clear int status
					rfm12_data( RFM12_CMD_TX | 0xaa);
				}

				break;
				
			} //end of switch

			//set the state machine to idle
			ctrl.rfm12_state = STATE_RX_IDLE;

#if RFM12_UART_DEBUG >= 2
			put_ch('F');
#endif

			rfm12_data( RFM12_CMD_FIFORESET | CLEAR_FIFO_INLINE);
			rfm12_data( RFM12_CMD_FIFORESET | ACCEPT_DATA_INLINE);

			no_fifo_reset:
				;
		}
	} while (recheck_interrupt);

#if RFM12_UART_DEBUG >= 2
		put_ch('E');
#endif

	//turn the int back on
	RFM12_INT_ON();
}

//enable internal data register and fifo
//setup selected band
#define RFM12_CMD_CFG_DEFAULT   (RFM12_CMD_CFG | RFM12_CFG_EL | RFM12_CFG_EF | RFM12_BASEBAND | RFM12_XTAL_LOAD)

//set rx parameters: int-in/vdi-out pin is vdi-out,
//Bandwith, LNA, RSSI
#define RFM12_CMD_RXCTRL_DEFAULT (RFM12_CMD_RXCTRL | RFM12_RXCTRL_P16_VDI | RFM12_RXCTRL_VDI_FAST | RFM12_FILTER_BW | RFM12_LNA_GAIN | RFM12_RSSI_THRESHOLD )

//set AFC to automatic, (+4 or -3)*2.5kHz Limit, fine mode, active and enabled
#define RFM12_CMD_AFC_DEFAULT  (RFM12_CMD_AFC | RFM12_AFC_AUTO_KEEP | RFM12_AFC_LIMIT_4 | RFM12_AFC_FI | RFM12_AFC_OE | RFM12_AFC_EN)

//set TX Power, frequency shift
#define RFM12_CMD_TXCONF_DEFAULT  (RFM12_CMD_TXCONF | RFM12_POWER | RFM12_TXCONF_FS_CALC(FSK_SHIFT) )

static const uint16_t init_cmds[] PROGMEM = {
	//defined above (so shadow register is inited with same value)
	RFM12_CMD_CFG_DEFAULT,

	//set power default state (usually disable clock output)
	//do not write the power register two times in a short time
	//as it seems to need some recovery
	(RFM12_CMD_PWRMGT | PWRMGT_DEFAULT),

	//set frequency
	(RFM12_CMD_FREQUENCY | RFM12_FREQUENCY_CALC(RFM12_FREQUENCY) ),

	//set data rate
	(RFM12_CMD_DATARATE | DATARATE_VALUE ),

	//defined above (so shadow register is inited with same value)
	RFM12_CMD_RXCTRL_DEFAULT,

	//automatic clock lock control(AL), digital Filter(!S),
	//Data quality detector value 3, slow clock recovery lock
	(RFM12_CMD_DATAFILTER | RFM12_DATAFILTER_AL | 3),

	//2 Byte Sync Pattern, Start fifo fill when sychron pattern received,
	//disable sensitive reset, Fifo filled interrupt at 8 bits
	(RFM12_CMD_FIFORESET | RFM12_FIFORESET_DR | (8 << 4)),

	//defined above (so shadow register is inited with same value)
	RFM12_CMD_AFC_DEFAULT,

	//defined above (so shadow register is inited with same value)
	RFM12_CMD_TXCONF_DEFAULT,

	//disable low dutycycle mode
	(RFM12_CMD_DUTYCYCLE),

	//disable wakeup timer
	(RFM12_CMD_WAKEUP),

	//enable rf receiver chain, if receiving is not disabled (default)
	//the magic is done via defines
	(RFM12_CMD_PWRMGT | PWRMGT_RECEIVE),
};

void phy_init()
{
	// initialize spi
	SS_RELEASE();
	DDR_SS |= (1<<BIT_SS);
	spi_init();

	// init buffer pointers
	ctrl.buffer_rx_in = 0;
	ctrl.buffer_rx_out = 0;
	ctrl.escape_byte_received = 0;


#if RFM12_LIVECTRL
	// init shadow registers with values about to be written to rfm12
	ctrl.rxctrl_shadow = RFM12_CMD_RXCTRL_DEFAULT;
	ctrl.afc_shadow = RFM12_CMD_AFC_DEFAULT;
	ctrl.txconf_shadow = RFM12_CMD_TXCONF_DEFAULT;
	ctrl.cfg_shadow =    RFM12_CMD_CFG_DEFAULT;
#endif

	// write all the initialisation values to rfm12
	uint8_t x;

	for (x = 0; x < ( sizeof(init_cmds) / 2) ; x++) {
		rfm12_data(pgm_read_word(&init_cmds[x]));
	}

	RFM12_INT_SETUP();

	//clear int flag
	rfm12_read(RFM12_CMD_STATUS);

	RFM12_INT_FLAG = (1<<RFM12_FLAG_BIT);

	//init receiver fifo, we now begin receiving.
	rfm12_data(CLEAR_FIFO);
	rfm12_data(ACCEPT_DATA);

	//activate the interrupt
	RFM12_INT_ON();
}

phy_carrier_status_t phy_carrier_status()
{
	return (rfm12_read(RFM12_CMD_STATUS) & RFM12_STATUS_RSSI) ? CARRIER_BUZY:CARRIER_FREE;
}

phy_transmitter_status_t phy_tx(void *data, uint8_t length)
{
	if (length > RFM12_TX_BUFFER_SIZE)
		return TRANSMITTER_ERROR;

	if (tx_buffer.status != BUFFER_FREE)
		return TRANSMITTER_OCCUPIED;

	memcpy(tx_buffer.data, data, length);
	tx_buffer.length = length;

	// schedule packet for transmission
	tx_buffer.status = BUFFER_BUZY;
	
	// disable the interrupt (as we're working
	// directly with the transceiver now) we won't lose
	// interrupts, as the AVR caches them in the int
	// flag. We could disturb an ongoing reception, if
	// it has just started some cpu cycles ago (as the 
	// check for this case is some lines (cpu cycles) above)
	// anyhow, we MUST transmit at some point...
	RFM12_INT_OFF();
	
	rfm12_data(RFM12_CMD_PWRMGT | PWRMGT_DEFAULT );
	
	// Update the state machine
	ctrl.num_bytes = tx_buffer.length;
	ctrl.bytecount = 0; //reset byte sent counter
	ctrl.rfm12_state = STATE_TX;

	// fill 2byte 0xAA preamble into data register
	// the preamble helps the receivers AFC circuit
	// to lock onto the exact frequency (hint: the
	// tx FIFO [if el is enabled] is two staged, so 
	// we can safely write 2 bytes before starting)
	rfm12_data(RFM12_CMD_TX | PREAMBLE);
	rfm12_data(RFM12_CMD_TX | PREAMBLE);
	
	rfm12_data(RFM12_CMD_PWRMGT | PWRMGT_DEFAULT | RFM12_PWRMGT_ET);
	
	//enable the interrupt to continue the transmission
	RFM12_INT_ON();

	return TRANSMITTER_ENQUEUED;
}

phy_receiver_status_t phy_rx(void *data, uint8_t *length)
{
	if (rx_buffer[ctrl.buffer_rx_out].status != BUFFER_COMPLETED)
		return RECEIVER_NOTHING;

	memcpy(length, &rx_buffer[ctrl.buffer_rx_out].length, 1);
	memcpy(data, rx_buffer[ctrl.buffer_rx_out].data, *length);

	rx_buffer[ctrl.buffer_rx_out].status = BUFFER_FREE;
	ctrl.buffer_rx_out ^= 1;
	
	return RECEIVER_COMPLETE;
}

