#ifndef _PHY_H
#define _PHY_H

// Physical layer is an interface between the program/network stack and the
// rfm12. Its role is to define the carrier to use and its related functions,
// like transmitting over the carrier, receiving from the carrier or detecting
// whether the carrier is free to use.

// Rewritten rfm12 library to suit the PHY/DLL/NET/TRAN/APP architecture
// better. Before, the MAC/PHY/DLL was strongly interleaved

#ifdef __PLATFORM_LINUX__
#include <stdint.h>
#endif
#include <inttypes.h>

#include "rfm12_config.h"

#include "include/rfm12_core.h"

#include "mac.h"

enum phy_buffer_status_t {
	BUFFER_FREE = 0,
	BUFFER_BUZY,
	BUFFER_COMPLETED
};

enum phy_carrier_status_t {
	CARRIER_FREE = 0,
	CARRIER_BUZY
};

enum phy_transmitter_status_t {
	TRANSMITTER_ERROR = 0,	// The data is longer than the internal buffer
	TRANSMITTER_OCCUPIED, 	// The transmit buffer is already occupied
	TRANSMITTER_ENQUEUED	// The packet has been enqueued successfully
};

enum phy_receiver_status_t {
	RECEIVER_NOTHING,		// Nothing has been received yet
	RECEIVER_COMPLETE		// Something is in the receiver buffer
};

typedef struct {
	phy_buffer_status_t status;
	uint8_t length;
	uint8_t data[RFM12_TX_BUFFER_SIZE];
} phy_buffer_t;

/** This data structure keeps all control and some status related variables.
 * By using a central structure for all global variables, the compiler can
 * use smaller instructions and reduce the size of the binary.
 *
 * \note Some states are defined in the non-documented rfm12_core.h header file.
 * \see ISR(RFM12_INT_VECT, ISR_NOBLOCK)
 */
typedef struct {
	// This controls the library internal state machine.
	volatile uint8_t rfm12_state;

	// Number of bytes to transmit.
	uint8_t num_bytes;

	// Counter for the bytes we are transmitting or receiving at the moment.
	uint8_t bytecount;
	
	// the number of the current in receive buffer.
	uint8_t buffer_rx_in;
	
	// the number of the current out receive buffer.
	uint8_t buffer_rx_out;
	
	uint8_t escape_byte_received;

#if RFM12_LIVECTRL
	uint16_t rxctrl_shadow;
	uint16_t afc_shadow;
	uint16_t txconf_shadow;
	uint16_t cfg_shadow;
#endif

} rfm12_control_t;

// Function to initialize the rfm12 device. To call everytime
void phy_init();

// Function checks the carrier and returns whether the carrier is free or not.
phy_carrier_status_t phy_carrier_status();

// Transmit data through the rfm12 device.
// Param:
// 	uint8_t*	data	the data to transmit
// 	uint8_t		length	the length of data to transmit
// The data is safe to be used/manipulated as this function memcopies the data ptr
// into its current buffer.
phy_transmitter_status_t phy_tx(void *data, uint8_t length);

// Receive data from the rfm12.
// Param:
// 	void*		data	the received data
// 	uint8_t*	length	the length of received data
// The data is safe to be used/manipulated by upper layers. This function memcopies 
// its current buffer into the data pointer. Same with length
phy_receiver_status_t phy_rx(void *data, uint8_t *length);

#if RFM12_USE_RX_CALLBACK
// Sets the callback function pointer
// Param:
//	void in_func(void*, uint8_t)
// where:
// 	void*		data	the received data
// 	uint8_t		length	the length of received data
// The data is NOT safe to be used/manipulated unless you do a memcopy as data will
// currently point to the physical layer's buffer.
void phy_rx_setCallback(void(*in_func)(void *data, uint8_t length));
#endif

// include headers for all the optional stuff in here
// this way the user only needs to include rfm12.h
#include "include/rfm12_extra.h"
#include "include/rfm12_livectrl.h"

#endif /* _PHY_H */
