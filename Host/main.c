/*
 * main.c
 *
 *  Created on: Nov 23, 2016
 *      Author: lsk95
 */

#include <stdio.h>
#include <stdlib.h>
#include "tran.h"
#include "rs232.h"

#define DELAY_US 100000
#define KEY_IN_BUFLEN   256


void print_serial(void);
void action1(char menu,char destaddr);

#define SER_IN_BUFLEN   4096
int cport_nr=5; /* /dev/ttyUSB0 */
unsigned char serbuf[SER_IN_BUFLEN];

//client

int main()
{
	//user interface
	setbuf(stdout, NULL);
	/* /dev/ttyS0 (COM1 on windows) */
	int bdrate=9600;       /* 9600 baud */
	char mode[]={'8','N','1',0};
	char appdata[512];

	char menu , destaddr=0;

	if(RS232_OpenComport(RS232_GetPortnr("COM6"), bdrate, mode))
	{
		printf("Can not open comport %d\n",cport_nr);
		return 0;
	}
	else
		printf("Chat application running");


	while(1)
	{

		printf("\n\t--------Chat Menu---------\t\n");
		printf("\n\n    1.\tSet Destination Address\n");
		printf("    2.\tStart Connection\n");
		printf("    3.\tSend Message\n");
		printf("    (press q to return to previous page)\n");
		menu=getchar();

		switch (menu)
		{
		case '1':
			printf("\n\t SK  \t(1) \n\t ZZ  \t(2) \n\t Jimmy  (3) \n");
			action1(menu,destaddr);
			break;
		case '2':
			break;
		case '3':
			while(1)
			{
				do
				{
					printf("Type the message you want to sent\n");
					fgets(appdata,512,stdin);
					RS232_cputs(cport_nr,appdata);
					printf("message : %s \n",appdata);
				}while(menu!='0');
				break;
			}
			break;
		default:
			printf("\n no mode selected \n");

		}

	}


	//	tran_segment *segment=socket_create(6,3,TCP);
	//
	//	tran_tx(appdata,sizeof(appdata),segment);
	//	tran_rx(segment,sizeof(tran_segment));
	//
	//	close(segment);

	//send and receive here

}


void print_serial(void)
{
	int n;

	n = RS232_PollComport(cport_nr, serbuf, SER_IN_BUFLEN-1);

	if(n > 0)
	{
		serbuf[n] = 0;   /* always put a "null" at the end of a string! */
		printf("%s", (char *)serbuf);
		fflush (stdout);
	}
}

void action1(char menu,char destaddr)
{
	do
	{
		while((destaddr=getchar())!='\n'){
			switch (destaddr)
			{
			case '1':
				RS232_SendByte(cport_nr,destaddr);
				printf("SK selected");
				menu=0;
				break;
			case '2':
				RS232_SendByte(cport_nr,destaddr);
				printf("ZZ selected");
				menu=0;
				break;
			case '3':
				RS232_SendByte(cport_nr,destaddr);
				printf("Jimmy selected");
				menu=0;
				break;
			default:
				printf("no destination selected");
			}
		}
	}while(menu!=0);
}
