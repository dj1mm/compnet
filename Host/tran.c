/*
 * tran.c
 *
 *  Created on: Nov 18, 2016
 *      Author: lsk95
 */

#include "tran.h"


//send to network
void tran_tx(const void *app_data, int app_len,tran_segment *segment)
{
	int seg_remain;
	int final_seg_len;
	int app_data_pos=0;

	if (app_len > MAX_PAYLOAD_SIZE)
	{
		segment->header.settings.multiple_data=true;
		seg_remain=app_len/MAX_PAYLOAD_SIZE;
		final_seg_len=app_len%MAX_PAYLOAD_SIZE;
	}

	while(segment->header.settings.multiple_data==1)
	{
		if (seg_remain==0)
		{
			memcpy(segment->app_data,app_data+app_data_pos,final_seg_len);
			segment->app_length=final_seg_len;
			segment->checksum=checksum(segment);
			printf("final string store:%s,data_pos:%d\n",segment->app_data,app_data_pos);
			/*
			   call network function
			 */
			segment->header.settings.multiple_data=false;
			//use final_seg_length
		}
		else
		{
			memcpy(segment->app_data,app_data+app_data_pos,MAX_PAYLOAD_SIZE);
			segment->app_length=MAX_PAYLOAD_SIZE;
			segment->checksum=checksum(segment);
			app_data_pos+=MAX_PAYLOAD_SIZE;
			printf("string store:%s,data_pos:%d,seg remain=%d\n",segment->app_data,app_data_pos,seg_remain);
			/*
				call network function
			 */
		}
		seg_remain--;
	}
	memcpy(segment->app_data,app_data,MAX_PAYLOAD_SIZE);
	segment->checksum=checksum(segment);
	/*
		   call network function
	 */
}

uint16_t checksum(tran_segment *segment)
{
	int sum=0;
	//header
	sum=sum+((segment->header.flow_control.data_ack)<<4)+segment->header.flow_control.seq_num;
	sum=sum+((segment->header.settings.connection_type)<<7)+((segment->header.settings.is_reliable)<<6);
	sum=sum+((segment->header.settings.err_correction)<<5)+((segment->header.settings.multiple_data)<<4);
	sum=sum+segment->src_port+segment->dest_port+segment->app_length;
	//app_data
	for (int k=0;k<sizeof(segment->app_data);k++)
	{
		sum+=segment->app_data[k];
	}

	printf("checksum value=%d\n",sum);
	return sum;
}

//assign src , addr port
tran_segment *socket_create(uint8_t src_port,uint8_t dest_port,int protocol)
{
	tran_segment *segment=(tran_segment*)malloc(sizeof(tran_segment));

	memset(segment,(int)NULL,sizeof(tran_segment));

	segment->src_port=src_port;
	segment->dest_port=dest_port;
	segment->header.settings.err_correction=true;

	switch(protocol)
	{
	case UDP:
		segment->header.settings.connection_type=false;
		segment->header.settings.is_reliable=false;
		break;
	case UDP_w_ack:
		segment->header.settings.connection_type=false;
		segment->header.settings.is_reliable=true;
		break;
	case TCP:
		segment->header.settings.connection_type=true;
		segment->header.settings.is_reliable=true;
		break;
	default:
		segment->header.settings.connection_type=false;
		segment->header.settings.is_reliable=false;
	}
	return segment;
}

//receive from network
void tran_rx(const void *net_data,int net_len)
{
	tran_segment buffer;
	memcpy(&buffer,((tran_segment*)net_data),net_len);
	printf("%s",buffer.app_data);
	if(buffer.checksum == checksum(&buffer))
	{
		printf("%d checksum ok",buffer.checksum);
	}
}

void close(tran_segment *segment)
{
	free(segment);
}
