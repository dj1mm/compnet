/*
 * tran.h
 *
 *  Created on: Nov 18, 2016
 *      Author: lsk95
 */

#ifndef TRAN_H_
#define TRAN_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define MAX_PAYLOAD_SIZE 30

enum {UDP=0,UDP_w_ack,TCP}protocol;

typedef struct{

	struct segment_control{

		struct sequencing{

			uint8_t seq_num:4;

			uint8_t data_ack:4;
		}flow_control;
		struct settings{
			// connection oriented = 1 , connection-less = 0
			bool connection_type:1;
			// reliable = 1 , unreliable = 0
			bool is_reliable:1;
			// have err correction = 1 , no err correction =0
			bool err_correction:1;
			// data broken into multiple segment = 1 , full data in one segment = 0
			bool multiple_data:1;


		}settings;
	} header;
	uint8_t src_port;
	uint8_t dest_port;
	uint8_t app_length;
	char app_data[MAX_PAYLOAD_SIZE];
	uint16_t checksum;


} tran_segment;


void tran_tx(const void *app_data, int app_len,tran_segment *segment);
void tran_rx(const void *net_data,int net_len);
uint16_t checksum(tran_segment *segment);
tran_segment *socket_create(uint8_t src_port,uint8_t dest_port,int protocol);
void close(tran_segment *segment);


// socket API
// socket,bind,connect,listen,accept,send,receive,close


#endif /* TRAN_H_ */
