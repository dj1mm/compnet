# Modified by Domenico Balsamo

TRG	= rfm12b
SRC	= main.cpp phy.cpp mac.cpp llc.cpp
SUBDIRS	= common rfm12lib

PRGER		= usbasp
MCU_TARGET	= atmega644p
MCU_FREQ	= 12000000

DEFS	+= -D__PLATFORM_AVR__
LIBS	+= -lm

EFUSE	= 0xFF
HFUSE	= 0x9C
LFUSE	= 0xE7

include Makefile_AVR.defs


