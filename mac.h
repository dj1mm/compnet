#ifndef _MAC_H
#define _MAC_H

// Mac layer
// Following the same logic, the primitives here can be used to
// transmit and receive at the MAC layer, by offering the access to
// the medium required from the upper layer (i.e. the required CSMA
// protocol), exploiting the service offered by the physical layer.
//
// The Mac layer is responsible of
//  - transmitting packets from the link layer over the rfm module
// 	- carrier collision detection and avoidance
// 	- receiving packets and passing those to the link layer
// 	- rejecting packets if not destined to us
// 	- stuffing/destuffing special bytes to prevent errors
// 	- determining the address of the device

#ifdef __PLATFORM_LINUX__
#include <stdint.h>
#endif
#include <inttypes.h>

#define FRAME_HEADER_BYTE 0x7e
#define FRAME_FOOTER_BYTE 0x7e
#define FRAME_ESCAPE_BYTE 0x7d

// Get the mac address specific to the device
inline uint8_t mac_address();

// Use the 0-persistent approach.
// Param:
// 	void*	mac_sdu		the service data unit to transmit
//	uint8_t	length		the length of data to be txed
void mac_0persistent_csma_request(void *mac_sdu, uint8_t length);

// Use the 1-persistent approach
void mac_1persistent_csma_request(void *mac_sdu, uint8_t length);

// Use the p-persistent approach
void mac_ppersistent_csma_request(void *mac_sdu, uint8_t length);

// A frame has been received with mac_sdu.
void mac_indication(void *mac_sdu, uint8_t length);

#endif /* _MAC_H */
