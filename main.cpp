/** \file main.c
 * \author Domenico Balsamo
 * \version 1.0
 * \date 2016/12/10
 */


#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "phy.h"
#include "uart.h"

void RxCallback(uint8_t *data, uint8_t length) {
	for(int i = 0; i < length; i++)
		put_ch(data[i]);
	put_ch('\r');
}

#define VERSION 1
int main(void)
{
	init_uart0();
	_delay_ms(100);
	phy_init();
	_delay_ms(100);
	sei();

	phy_init();
	int i=0;
	char* str = "HI";
#if VERSION == 1 | !defined(VERSION)
	char* rx = "";
	while(1)
	{
		if(i++ >9999){
			phy_tx(str,2);
			i = 0;
		}
	}
#else
	phy_rx_setCallback(RxCallback);
	
	while(1)
	{
		if(i++ >9999){
			phy_tx(str,2);
			i = 0;
		}
	}
#endif
}


