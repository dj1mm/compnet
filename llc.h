// LLC layer
// The following primitives can be used to transmit and receive at the
// LLC layer, exploiting the service (i.e. medium access) offered by
// the layer above (MAC) .
//
// The llc layer is responsible of
//  - spliting packets into frames and reconstructing those into packets
// 	- managing frames received from different devices and in different orders
// 	- 

#ifdef __PLATFORM_LINUX__
#include <stdint.h>
#endif
#include <inttypes.h>

// A request to transmit should include the source and the destination
// address, the Service Data Unit (SDU) at the LLC layer, which can
// also be null if the packet is an acknowledgement (ACK), the CSMA
// required to the layer below (MAC) and ACK status.
void llc_tran_request(uint8_t source_addr, uint8_t dest_addr, void *llc_sdu, int csma_mode, uint8_t ack);

// A packet has been received with llc_sdu. The uint8_t returns a status
// information to the client (LLC layer).
uint8_t llc_tran_indication(void *llc_sdu, uint8_t ack);

// Returns 1 if primitive correctly processed, 0 otherwise
uint8_t llc_tran_confirm(uint8_t *failure_code);

// The following primitives are useful to enable a connection-
// oriented service
// A request to establish the communication should include the source and
// the destination address and the CSMA service required to the layer
// below (MAC).
void llc_establish_request(uint8_t source_addr, uint8_t dest_addr, int csma_mode);

// A request for communicating has been received. The int returns a
// status information to the client (LLC layer).
uint8_t llc_establish_indication(uint8_t source_addr, uint8_t dest_addr);

// This is the response to a request of establish communication.
uint8_t llc_establish_response(uint8_t source_addr, uint8_t dest_addr, int csma_mode, int communication_response);

// Returns 1 if the communication is established, 0 otherwise,
uint8_t llc_establish_confirm(uint8_t source_addr, uint8_t dest_addr, int communication_response);


